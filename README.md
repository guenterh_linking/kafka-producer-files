# Kafka Producer: Files

A producer which takes a directory with text files and creates one message per line for each file found.
It automatically recognizes .gz and .bz2 extensions and uses the decompression to read these files.

A special mode can be used for sorted n-triple files where each triple with the same subject is
concatenated to the same message as valid n-triple format. Only works for **pre-sorted** n-triple files!

