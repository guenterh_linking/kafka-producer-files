package org.swissbib.linked

import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream
import org.apache.logging.log4j.Logger
import java.io.BufferedReader
import java.io.IOException
import java.nio.file.Files
import java.nio.file.Path
import java.util.regex.Pattern
import java.util.zip.GZIPInputStream

class FileReader(private val properties: PropertiesLoader, private val packTriples: Boolean, private val skipHead: Boolean, private val log: Logger) {
    private val triple = Regex("<(.*?)> .*")
    private var lineCount: Int = 0

    private fun open(path: Path): BufferedReader {
        return if (Files.isRegularFile(path)) {
            val file = path.toFile()
            log.info("Create buffered reader for $file.")
            when {
                path.toString().endsWith(".gz") -> {
                    GZIPInputStream(file.inputStream()).bufferedReader(Charsets.UTF_8)
                }
                path.toString().endsWith(".bz2") -> {
                    BZip2CompressorInputStream(file.inputStream()).bufferedReader(Charsets.UTF_8)
                }
                else -> file.inputStream().bufferedReader(Charsets.UTF_8)
            }
        } else {
            throw IOException("Could not read file at $path.")
        }
    }

    private val datePattern = Pattern.compile("(?<date>\\d{4}-\\d{2}-\\d{2})")
    private fun getDate(path: Path): String? {
        val matcher = datePattern.matcher(path.toString())
        return if (matcher.find()) {
            matcher.group("date")
        } else {
            null
        }
    }

    fun read(path: Path) {
        log.info("Reading file $path")
        properties.kafkaProperties["client.id"] = "read-${path.toString().split('/').last()}"
        val producer = Producer(properties.kafkaProperties, getDate(path), properties.appProperties["kafka.topic"] as String, log)
        val reader = open(path)
        if (skipHead) log.info("Skip first line: ${reader.readLine()}")
        if (packTriples) produceResources(reader, producer)
        else produceLines(reader, producer)
        reader.close()
        log.info("Created $lineCount messages from file $path.")
    }

    private fun produceResources(reader: BufferedReader, producer: Producer) {
        var currentSubject: String? = null
        var resource = ""

        for (line in reader.lines()) {
            if (line.isNullOrEmpty())
                continue
            val match = triple.matchEntire(line)
            val value = match?.groups?.get(1)?.value
            if (!value.isNullOrEmpty()) {
                when (currentSubject) {
                    null -> {
                        currentSubject = value
                        resource = line + "\n"
                    }
                    value -> { // currentSubject == value
                        resource += line + "\n"
                    }
                    else -> { // currentSubject != value
                        producer.send(resource.trim())
                        val message = "packed triple package was sent to kafka / number of packed lines read so far: $lineCount"
                        log.info(message)

                        currentSubject = value
                        resource = line + "\n"
                        lineCount += 1
                    }
                }
            }

        }
        // Send last resource
        producer.send(resource.trim())
    }

    private fun produceLines(reader: BufferedReader, producer: Producer) {
        for (line in reader.lines()) {
            producer.send(line)
            lineCount += 1
        }
    }
}