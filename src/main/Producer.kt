package org.swissbib.linked

import org.apache.kafka.clients.producer.KafkaProducer
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.logging.log4j.Logger
import org.swissbib.SbMetadataModel
import java.util.*

class Producer(props: Properties, private val date: String?, private val topic: String, val log: Logger) {

    private val instance = KafkaProducer<String, SbMetadataModel>(props)

    fun send(message: String) {
        instance.send(ProducerRecord<String, SbMetadataModel>(topic, SbMetadataModel()
                .setData(message)
                .setMessageDate(date)))
    }
}