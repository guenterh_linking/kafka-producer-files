package org.swissbib.linked

import org.apache.logging.log4j.Logger
import java.io.FileInputStream
import java.io.FileOutputStream
import java.nio.charset.Charset
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.util.concurrent.TimeUnit
import kotlin.system.exitProcess

class FileWatcher(private val log: Logger) {

    private val propertiesLoader = PropertiesLoader(log)
    private val appProperties = propertiesLoader.appProperties

    private val packTriples = appProperties.getProperty("app.packTriples")!!.toBoolean()
    private val skipHead = appProperties.getProperty("app.skipHead")!!.toBoolean()
    private val watch = appProperties.getProperty("app.watch")!!.toBoolean()

    private val queue = Paths.get(appProperties.getProperty("app.persist.queue") + "/read_files.txt")

    private val reader = FileReader(propertiesLoader, packTriples, skipHead, log)

    private val source: Path = Paths.get(appProperties["app.dir"] as String)
    private val hasReadPath = mutableSetOf<Path>()

    fun run() {
        if (!Files.isDirectory(source)) {
            log.error("Not a valid directory: $source.")
            exitProcess(1)
        }
        if (watch) {
            // keep watching the source directory for new files.
            // will not re-read modified files.
            log.info("Watching files in path $source.")
            while (true) {
                val files = source.toFile().listFiles()
                if (files != null) {
                    hasReadPath.clear()
                    val metaFileReader = FileInputStream(queue.toFile())

                    for (line: String in metaFileReader.bufferedReader(Charset.defaultCharset()).lines()) {
                        val path = Paths.get(line)
                        if (Files.exists(path)) {
                            hasReadPath.add(path)
                        } else {
                            log.info("No file for path $path exists! Ignore it.")
                        }
                    }
                    metaFileReader.close()

                    log.info("Will read files from path: $source")
                    log.info("Already read files: $hasReadPath")

                    for (file in files) {
                        if (file.isFile && !hasReadPath.contains(file.toPath())) {
                            try {
                                reader.read(file.toPath())
                            } catch (ex: Exception) {
                                log.error(ex.message)
                                log.error("Exiting program due to error! Do no attempt to read the file ${file.toPath()} again!")
                                hasReadPath.add(file.toPath())
                                writeRememberFile()
                                exitProcess(1)
                            }
                            hasReadPath.add(file.toPath())
                        }
                    }
                    hasReadPath.removeIf {
                        !Files.exists(it)
                    }
                    log.info("New files read $hasReadPath.")
                    writeRememberFile()
                } else {
                    log.info("No files present.")
                }
                TimeUnit.SECONDS.sleep(10L)
            }
        } else {
            // read all files once and call it a day.
            val files = source.toFile().listFiles()
            if (files != null) {

                log.info("Reading files $files once!")
                // all files are read in parallel. So the order will not be preserved.
                files.map {
                    reader.read(it.toPath())
                }
            } else {
                log.warn("Found no files to read! Have you mounted the correct data directory?")
            }
            log.info("Finished reading all files! Exit process...")
        }
    }
    fun writeRememberFile() {
        val writer = FileOutputStream(queue.toFile()).bufferedWriter(Charset.defaultCharset())
        for (path in hasReadPath) {
            writer.write(path.toString())
            writer.newLine()
        }
        writer.flush()
        writer.close()
    }
}